<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */



define('DB_LOCAL', true);



// ** MySQL settings - You can get this info from your web host ** //



if (defined("DB_LOCAL")) {
    /** The name of the database for WordPress */
    define('DB_NAME', 'b003');

    /** Имя пользователя MySQL */
    define('DB_USER', 'root');

    /** Пароль к базе данных MySQL */
    define('DB_PASSWORD', '');

} else {
    /** The name of the database for WordPress */
    define('DB_NAME', 'readrus_main');

    /** Имя пользователя MySQL */
    define('DB_USER', 'readrus_main');

    /** Пароль к базе данных MySQL */
    define('DB_PASSWORD', '123abcd123');
}

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '1IddYXsM=6p{i,Ya6e@vtH!Bf^#Gd3DZia9kR3S(zeP#,GQN]pN~^J@JTxk3D<RK');
define('SECURE_AUTH_KEY',  'A}p5u%!`U[YI0,0u`thwe~:g%fPvXAgJ!#5rIs(([lU!>k:C]@~T<p9@ Yf:+%og');
define('LOGGED_IN_KEY',    'M2{JNf2(C!an)?S`o0K#IK ]4:gy:Zs4oHqAs`NE)^U7o0Zu,6a|YyEIV4(Pvoz{');
define('NONCE_KEY',        'N7ao7/`}UupH_tDIy=Ty$sR|-|XekRId)JLB<) o)q]S>)JYn{lr) h3`vk{BmFc');
define('AUTH_SALT',        'e,!,O|:BGE$T.+eeC}<WTF]N2.)C(EZfh7^EgG CU9cO,3JkU/a7j5HmF|jwfYem');
define('SECURE_AUTH_SALT', 's&&)zRum(j)s+Y:di0Lv0nivuDL~@U4JJhk!G1q{=YiJq MxZ3X_wvugHi_J^!pw');
define('LOGGED_IN_SALT',   'fiCR?/h=8e$?omRD)p&djOpzqiR?WuHmj;v:QT/Or9ux1M9+Hq?0Rt7U4=zxFt$a');
define('NONCE_SALT',       '3|+|3Es@{Q,8u~C>=^s!QCaa_]M)F%C#SGRqx!4-DEWm=xP}oIHC)VYU8~x#i{/*');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'b003';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
