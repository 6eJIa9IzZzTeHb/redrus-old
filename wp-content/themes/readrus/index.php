<?php get_header(); ?>


<?php include(__DIR__.'/parts/menu.php')?>



<?php if (is_single()):?>



    <?php include(__DIR__.'/parts/post-'.get_post_type().'.php')?>


<?php else: ?>

    <?php include(__DIR__.'/parts/content.php')?>

<?php endif; ?>



<?php get_footer();?>