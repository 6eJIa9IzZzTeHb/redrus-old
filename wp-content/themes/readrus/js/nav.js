$(function() {
    var blockMenu = $('#l-nav');
    var buttonMenu = $('#buttonMenu');

    buttonMenu.on('click', function(){
        if(blockMenu.hasClass('active')){
            blockMenu.removeClass('active');
            $('#menu-toggle').removeClass('active');
            $(this).removeClass('active');
        }
        else{
            blockMenu.addClass('active');
            $('#menu-toggle').addClass('active');
            $(this).addClass('active');
        }
    });


    $(document).click( function(event){
        if(document.documentElement.clientWidth < 1200) {
            if ($(event.target).closest(blockMenu).length) {

            }
            else {
                blockMenu.removeClass('active');
                $('#menu-toggle').removeClass('active');
                buttonMenu.removeClass('active');
            }
        }
    });


    (function() {
        function resizeNavTop() {
            var mxScrollTop = 400;
            var scrollTop = $(window).scrollTop();

            var perc = Math.max(0, mxScrollTop - scrollTop) / mxScrollTop;

            $('.nav-top').toggleClass('in-top', scrollTop < mxScrollTop);

        }

        resizeNavTop();
        $(window).on('scroll', function () {
            resizeNavTop();
        });
        $(window).on('resize', function () {
            resizeNavTop();
        });

    })();

    $('.checkbox').each(function() {
        var ch = $(this);
        var input = ch.find('[type=checkbox]');

        var refreshChBox = function() {
            var active = input.is(':checked');
            ch.find('.check').toggleClass('active', active);
        };
        refreshChBox();


        input.change(function() {
            refreshChBox();
        });
    })
});