jQuery(function($) {

    function getSettings(el, plugin, defaultSettigns) {
        var settings = el.data(plugin);

        if (typeof(settings) != 'object') {
            settings = {};
        }


        if (typeof(defaultSettigns) != 'undefined') {
            $.extend(defaultSettigns, settings);
        }


        return defaultSettigns;
    }

    $('[data-owl]').each(function() {

        var el = $(this);


        var autoItems = Math.max(1, Math.min(4, el.find('.book').length-1));

        if ($(document).width() < 980) {
            autoItems = 2;
        }
        if ($(document).width() < 755) {
            autoItems = 1;
        }




        var options = getSettings(el, 'owl', {
            loop:true,
            nav:true,
            navText: ['<span class="fa fa-angle-left arrow bg-cream"></span>', '<span class="fa fa-angle-right arrow bg-cream"></span>'],
            autoplay:true,
            margin:10,
            items: autoItems
        });




        el.owlCarousel(options);
    });



    $('[data-toggle=more]').click(function() {

        var el = $(this);

        el.parent().parent().find('.none').removeClass('none');
        el.parent().addClass('none');

    });
});