<?php

$enabledShortCodes = [
    'rating',
    'breadcrumbs',
    'menu',
    'adv_on_site',
    'dialogs',
    'login',
    'signup',
    'reset_password',
    'greatings',
    'settings',
    'my_publications',
    'renew_pass',
    'logout',
    'rules'
];

global $htmls;
$htmls = [];


function breadcrumbs_function( $attrs ) {
    ob_start();

    $menuItems = [];

    if (isset($attrs['items'])) {
        $menuItems = $attrs['items'];
    } else {
        $menuItems[]= ['name'=>'<span class="menu-icon-home text-vine f13 "></span>  <strong class="f13">Главная</strong>', 'link'=>'/'];

        if (isset($attrs['submenu'])) {
            $menuItems []= ['name'=>$attrs['submenu'], 'link'=>$attrs['submenu_link']];
        }

        $menuItems[]= get_the_title();

    }


    include __DIR__.'/shortcodes/breadcrumbs.php';

    $content = ob_get_contents();
    ob_end_clean();

    return $content;
}


function menu_function( $attrs ) {
    ob_start();

    $viewName = $attrs['view'];

    include __DIR__.'/parts/navs/submenus/'.$viewName.'.php';

    $content = ob_get_contents();
    ob_end_clean();

    return $content;
}


function rating_function( $attrs ) {
    ob_start();

    $file = null;

    switch ($attrs['type']) {
        case 'writers_basic':
            $file = __DIR__ . "/shortcodes/rating/writers.php";
            break;
        case 'book_previews':
            $file = __DIR__ . "/shortcodes/rating/book_previews.php";
            break;
        case 'book_news':
            $file = __DIR__ . "/shortcodes/rating/book_news.php";
            break;
        case 'book_authors':
            $file = __DIR__ . "/shortcodes/rating/book_authors.php";
            break;
        case 'reviews':
            $file = __DIR__ . "/shortcodes/rating/reviews.php";
            break;

        case 'events':
            $file = __DIR__ . "/shortcodes/rating/events.php";
            break;


    }

    include $file;

    $content = ob_get_contents();
    ob_end_clean();
    return $content;
}


function login_function() {
    global $htmls;
    return $htmls['login'];
}


function signup_function() {
    global $htmls;
    return $htmls['signup'];
}

function reset_password_function() {
    global $htmls;
    return $htmls['reset_password'];
}

function rules_function() {
    ob_start();

    $file = __DIR__ . "/shortcodes/rules.php";

    require($file);

    $content = ob_get_contents();
    ob_end_clean();
    return $content;
}


function adv_on_site_function() {
    ob_start();

    $file = __DIR__ . "/shortcodes/adv_on_site.php";

    require($file);

    $content = ob_get_contents();
    ob_end_clean();
    return $content;
}

function dialogs_function() {
    ob_start();

    $file = __DIR__ . "/shortcodes/dialogs.php";

    require($file);

    $content = ob_get_contents();
    ob_end_clean();
    return $content;
}



function logout_function() {
   return "";
}





foreach($enabledShortCodes as $code) {
    add_shortcode($code, $code.'_function');

    if (function_exists($code.'_form_controller')) {

        add_action('init', $code . '_form_controller');
    }
}


/***
 * AUTH
 */


function logout_form_controller( ) {
    if ($_SERVER['REQUEST_URI'] == '/log-out/') {
        wp_logout();
        wp_redirect(get_permalink(2));
        exit;

    }
}

function login_form_controller( ) {

    if ($_SERVER['REQUEST_URI'] == '/login/') {


        if (get_current_user_id()) {
            wp_redirect(get_permalink(2));
            exit;
        }

        ob_start();



        $remember = true;
        $loginOrEmail = "";
        $password = "";
        $error = [
            'loginOrEmail'=>'',
            'password'=>''
        ];

        if (isset($_SERVER['REQUEST_METHOD']) && $_SERVER['REQUEST_METHOD']=='POST') {

            $loginOrEmail = $_POST['login'];
            $password = $_POST['password'];
            $remember = $_POST['remember'];


            $userE = get_user_by( 'email', $loginOrEmail);
            $userL = get_user_by( 'login', $loginOrEmail);


            $user = $userE ? $userE : $userL;

            if (!$user) {
                $error['loginOrEmail'] = 'Пользователь не найден';

            } else {

                if (wp_check_password($password, $user->data->user_pass, $user->ID)) {
                    //login
                    $credentials = array();

                    $credentials['user_login'] = $user->user_login;
                    $credentials['user_password'] = $password;
                    $credentials['remember'] = $remember;
                    wp_signon($credentials);
                    wp_redirect(get_permalink(348).'?nickname='.$user->nickname);
                    exit;
                } else {
                    $error['password'] = 'Невереный пароль';
                }
            }

        }

        include __DIR__ . '/shortcodes/login.php';

        $content = ob_get_contents();
        ob_end_clean();

        global $htmls;
        $htmls['login'] = $content;
    }
}

function reset_password_form_controller( ) {

    if ($_SERVER['REQUEST_URI'] == '/reset-password/') {


        if (get_current_user_id()) {
            wp_redirect(get_permalink(2));
            exit;
        }

        ob_start();



        $loginOrEmail = "";
        $error = [
            'loginOrEmail'=>'',
            'password'=>''
        ];

        if (isset($_SERVER['REQUEST_METHOD']) && $_SERVER['REQUEST_METHOD']=='POST') {

            $loginOrEmail = $_POST['login'];


            $userE = get_user_by( 'email', $loginOrEmail);
            $userL = get_user_by( 'login', $loginOrEmail);


            $user = $userE ? $userE : $userL;

            if (!$user) {
                $error['loginOrEmail'] = 'Пользователь не найден';

            } else {
                die('Ошибка сервера');
            }

        }

        include __DIR__ . '/shortcodes/reset_password.php';

        $content = ob_get_contents();
        ob_end_clean();

        global $htmls;
        $htmls['reset_password'] = $content;
    }
}


function signup_form_controller( ) {

    if ($_SERVER['REQUEST_URI'] == '/sign-up/') {


        if (get_current_user_id()) {
            wp_redirect(get_permalink(2));
            exit;
        }

        ob_start();



        $remember = true;
        $login = "";
        $email = "";
        $fname = "";
        $sname = "";
        $password = "";
        $retry_password = "";
        $day = "";
        $month = "";
        $year = "";
        $city = "";
        $error = [
            'login'=>'',
            'email'=>'',
            'fname'=>'',
            'sname'=>'',
            'password'=>'',
            'retry_password'=>'',
            'day'=>'',
            'month'=>'',
            'year'=>'',
            'city'=>'',
            'remember'=>''
        ];

        if (isset($_SERVER['REQUEST_METHOD']) && $_SERVER['REQUEST_METHOD']=='POST') {

            foreach ($error as $key=>$value) {
                $$key = $_POST[$key];
            }

            $day = $_POST['birth_day'];
            $month = $_POST['birth_month'];
            $year = $_POST['birth_year'];


            $errors = false;

            if (!preg_match('/[a-zA-Z0-9\_]{3,12}/', $login)) {
                $error['login'] = "Должен быть 3-12 символов из латинских букв и цифр";
                $errors = true;
            }

            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $error['email'] = "Неверный email";
                $errors = true;
            }

            if (!preg_match('/[a-zA-Z0-9а-яА-Я\_\-]{2,20}/', $fname)) {
                $error['fname'] = "Должно быть 2-20 символов и состаять из букв и цифр";
                $errors = true;
            }

            if (!preg_match('/[a-zA-Z0-9а-яА-Я\_\-]{1,20}/', $sname)) {
                $error['sname'] = "Должна быть 1-20 символов и состаять из букв и цифр";
                $errors = true;
            }


            if (!preg_match('/[a-zA-Z0-9\_]{6,12}/', $password)) {
                $error['password'] = "Должен быть 6-12 символов из латинских букв";
                $errors = true;
            }

            if ($password != $retry_password) {
                $error['retry_password'] = "Пароли не совпадают";
                $errors = true;
            }

            if (!preg_match('/[a-zA-Z0-9а-яА-Я\_\ ]*/', $city)) {
                $error['city'] = "Неправильный формат города";
                $errors = true;
            }


            if (($day || $month || $year) &&
                (!is_numeric($day) || !is_numeric($month) || !is_numeric($year))) {
                $error['birth_day'] = "Выберите день месяц и год";
                $errors = true;

            }


            if (!$remember) {
                $error['remember'] = "Вы должны прочитать соглашение";
                $errors = true;

            }

            if (!$errors) {

                $userE = get_user_by( 'email', $email);
                $userL = get_user_by( 'login', $login);

                if ($userE) {
                    $error['email'] = "Email занят";
                }

                if ($userL) {
                    $error['login'] = "Логин занят";
                }

                $user_id = wp_create_user( $login, $password, $email );
                $birthday = (2018-$year).'-'.
                    ($month<10?'0':'').$month.'-'.
                    ($day<10?'0':'').$day;

                wp_update_user(['ID'=>$user_id,
                    'first_name'=>$fname,
                    'last_name'=>$sname]);

                update_field('birthday', $birthday, 'user_'.$user_id);
                update_field('city', $city, 'user_'.$user_id);

                $credentials['user_login'] = $login;
                $credentials['user_password'] = $password;
                $credentials['remember'] = $remember;
                wp_signon($credentials);
                wp_redirect(get_permalink(348).'?nickname='.$login);
                exit;
            }
        }


        include __DIR__ . '/shortcodes/signup.php';

        $content = ob_get_contents();
        ob_end_clean();

        global $htmls;
        $htmls['signup'] = $content;
    }
}
?>