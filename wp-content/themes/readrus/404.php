<?php /* Template Name: Landing */ ?>
<?php get_header(); ?>


<?php include(__DIR__ . '/parts/menu.php') ?>

    <main class="content m-20 pl-5 full-width">
        <div class="container">


            <div class="row">

                <div class="col-md-12">
                    <div class="bg-white pb60 pt60 landing-author-books-column full-height">
                    </div>

                </div>
                <div class="col-lg-6 hidden-md-down pr0-md">
                    <div class="bg-white pb20 pt20 landing-author-books-column full-height">


                        <img src="<?= get_template_directory_uri()?>/img/404/404-bars.png" alt=""/>



                    </div>
                </div>
                <div class="col-lg-6 p10-md pl0-lg">
                    <div class="bg-white pl80 pb20 pr80 full-height text-center text-dark-grey">

                        <img src="<?= get_template_directory_uri()?>/img/404/404.png" alt=""/>

                        <br/>
                        <br/>
                        <div>
                            К сожалению, страница, которую вы ищите, не существует на нашем сайте.
                        </div>

                        <br/>
                        <div>
                            Но вы можете вернуться на <a href="/" class="text-vine">Главную</a>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="bg-white pb100 pt100 landing-author-books-column full-height">
                    </div>

                </div>
            </div>

        </div>
    </main>

<?php get_footer(); ?>