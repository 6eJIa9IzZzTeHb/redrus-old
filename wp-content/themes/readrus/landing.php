<?php /* Template Name: Landing */ ?>
<?php get_header(); ?>


<?php include(__DIR__ . '/parts/menu.php') ?>


    <main class="content m-20 pl-5 full-width">
        <div class="container">


            <div class="row">
                <div class="col-md-12">

                    <div style="background-image:url('<?= of_get_option('main_top_image') ?>');" class="landing-main-top-image"></div>

                </div>
            </div>

            <nav class="row landing-menu">
                <div class="col-lg-12">
                    <div class="bg-white">
                        <div class="row">
                            <div class="col-xl-10 offset-xl-1">
                                <div class="bg-white">
                                    <ul class="row">
                                        <li class="col-lg-3 col-6">
                                            <a class="item"
                                               style="background-image: url('<?= of_get_option('main_writer_school') ?>')"
                                               href="#!">
                                                <span>Школа мастерства</span>

                                            </a>
                                        </li>
                                        <li class="col-lg-3 col-6">
                                            <a class="item"
                                               style="background-image: url('<?= of_get_option('main_author_books') ?>')"
                                               href="#!">
                                                <span>Произведения авторов</span>

                                            </a>
                                        </li>
                                        <li class="col-lg-3 col-6">
                                            <a class="item"
                                               style="background-image: url('<?= of_get_option('main_greatest_authors') ?>')"
                                               href="#!">
                                                <span>Великие писатели</span>

                                            </a>
                                        </li>
                                        <li class="col-lg-3 col-6">
                                            <a class="item"
                                               style="background-image: url('<?= of_get_option('main_events') ?>')"
                                               href="#!">
                                                <span>Конкурсы</span>

                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </nav>


            <div class="row">
                <div class="col-md-7 pr0-md">
                    <div class="bg-white pb20 landing-author-books-column full-height">
                        <h3 class="landing-category mb30">Произведения авторов</h3>



                        <?php require(__DIR__ . '/parts/landing/authors_books.php') ?>

                        <div>
                            <button class="btn-typical plr20 ptb10 vine-button">Смотреть больше &nbsp;&nbsp;&nbsp;<span class="fa fa-arrow-right f15"></span> </button>
                        </div>

                    </div>
                </div>
                <div class="col-md-5 pl0-md">
                    <div class="bg-white landing-author-news pb20 full-height">
                        <h3 class="landing-category pb20">Новые поступления</h3>


                        <?php require(__DIR__ . '/parts/landing/new_books.php') ?>

                        <div>
                            <button class="btn-typical plr20 ptb10 vine-button">Смотреть больше &nbsp;&nbsp;&nbsp;<span class="fa fa-arrow-right f15"></span> </button>
                        </div>
                    </div>
                </div>

            </div>


            <div class="row">
                <div class="col-md-12">

                    <div style="background-image:url('<?= of_get_option('main_top_image') ?>');" class="landing-main-top-image"></div>

                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="bg-white p20 full-height">
                        <h3 class="landing-category mb30" style="padding: 5px 45px;">
                            Анонсы произведений
                            <a href="#" class="btn btn-typical pull-sm-right">Перейти к разделу &nbsp;&nbsp;&nbsp;<span class="fa fa-arrow-right f15"></span></a>

                        </h3>

                        <div class="owl-carousel owl-with-buttons" data-owl>
                            <?php require(__DIR__ . '/parts/landing/anounce_slider.php') ?>
                        </div>




                    </div>
                </div>


            </div>


            <div class="row">
                <div class="col-md-12">
                    <div class="bg-white p20 full-height">
                        <h3 class="landing-category mb30" style="padding: 5px 45px;">
                            Статьи и заметки авторов
                            <a href="#" class="btn btn-typical pull-sm-right">Перейти к разделу &nbsp;&nbsp;&nbsp;<span class="fa fa-arrow-right f15"></span></a>

                        </h3>

                        <?php require(__DIR__ . '/parts/landing/articles.php') ?>



                    </div>
                </div>


            </div>



            <div class="row">
                <div class="col-md-12">

                    <div style="background-image:url('<?= of_get_option('main_top_image') ?>');" class="landing-main-top-image"></div>

                </div>
            </div>



            <div class="row ">
                <div class="col-md-7 pr0-md">
                    <div class="bg-white pb20  pt20 landing-author-books-column full-height">
                        <h3 class="landing-category mb30">Рейтинг авторов</h3>

                        <?php require(__DIR__ . '/parts/landing/authors.php') ?>

                        <div>
                            <button class="btn-typical plr20 ptb10 vine-button">Смотреть больше &nbsp;&nbsp;&nbsp;<span class="fa fa-arrow-right f15"></span> </button>
                        </div>

                    </div>
                </div>
                <div class="col-md-5 pl0-md">
                    <div class="bg-white pt20 landing-author-news pb20 full-height">
                        <h3 class="landing-category pb20">Рейтинг произведений</h3>


                        <?php require(__DIR__ . '/parts/landing/books_rating.php') ?>

                        <div>
                            <button class="btn-typical plr20 ptb10 vine-button">Смотреть больше &nbsp;&nbsp;&nbsp;<span class="fa fa-arrow-right f15"></span> </button>
                        </div>
                    </div>
                </div>


            </div>
        </div>

    </main>

<?php get_footer(); ?>