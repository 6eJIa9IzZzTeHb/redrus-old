<?php


$actionToFunction = [
    'admin_head'=>'admin_styles',
    'wp_enqueue_scripts'=>'frontend_styles',
];



function admin_styles() {

    // Load fonts
    wp_enqueue_style( 'readrus-fonts', get_template_directory_uri() . '/css/fonts.css' );

}


function frontend_styles() {

    // Load fonts
    wp_enqueue_style( 'readrus-fonts', get_template_directory_uri() . '/css/fonts.css' );

    // stylesheet
    wp_enqueue_style( 'readrus-site-css', get_template_directory_uri() . '/css/site.css' );
    wp_enqueue_style( 'readrus-elements-css', get_template_directory_uri() . '/css/elements.css' );
    wp_enqueue_style( 'readrus-owl-css', get_template_directory_uri() . '/assets/owl/owl.carousel.min.css' );

    // js head
    wp_enqueue_script( 'readrus-fa-css', 'https://use.fontawesome.com/8f689651b3.js', []);

    // js footer
    wp_enqueue_script( 'readrus-owl', get_template_directory_uri() . '/assets/owl/owl.carousel.min.js', [], false, true );
    wp_enqueue_script( 'readrus-main', get_template_directory_uri() . '/js/main.js', [], false, true);
    wp_enqueue_script( 'readrus-nav', get_template_directory_uri() . '/js/nav.js', [], false, true );

}


require_once(__DIR__ . "/shortcodes.php");


foreach ($actionToFunction as $action=>$functionName) {
    add_action($action, $functionName);
}



add_action('after_setup_theme', 'remove_admin_bar');

function remove_admin_bar() {
    if (!current_user_can('administrator') && !is_admin()) {
        show_admin_bar(false);
    }
}