<?php
/**
 * @var $remember bool
 * @var $loginOrEmail string
 * @var $password string
 */
?>
<div class="bg-white text-center ptb100 pull-left full-width">
    <h2 class="text-dark-grey mb40">Авторизация пользователя</h2>

    <form method="POST" class="mb20 typical-form">
        <input type="text" name="login" value="<?= $loginOrEmail?>" class="grey-input" placeholder="Логин или email"/>
        <div class="text-danger"><?= $error['loginOrEmail']?></div>
        <input type="password" name="password" value="<?= $password?>" class="grey-input mt20" placeholder="Пароль"/>
        <div class="text-danger"><?= $error['password']?></div>
        <div class="plr10 mb40 mt10">
            <a href="<?= get_permalink(266)?>" class="pull-left mt2">Забыли пароль?</a>
            <span class="pull-right" >
                <label class="checkbox hand">
                    <span class="mt3 inline-block vertical-top text-middle-grey">
                        Запомнить меня
                    </span>
                    <span class="check mt3 inline-block vertical-top">
                        <span class="check-ok fa fa-check relative"></span>
                        <span class="check-fail relative"></span>
                        <input name="remember" type="checkbox" <?= $remember?'checked':''?>/>
                    </span>
                </label>
            </span>
        </div>
        <br/>
        <button class="btn vine-button">Войти</button>
        <br/>
        <h5 class="mt20 text-dark-grey mt40">Нет аккаунта? Создайте его!</h5>
        <a href="<?= get_permalink(264)?>">Зарегестрироваться</a>
    </form>

</div>