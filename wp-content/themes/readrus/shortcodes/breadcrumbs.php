<?php
/**
 * @var $menuItems []
 */

$first = true;
?>
<nav class="bg-cream p10 plr50 relative box-shadow-grey1 z2 pull-left full-width">


    <?php foreach($menuItems as $item): ?>

        <?php if (!$first): ?>
            <span class="fa fa-angle-right mlr10"></span>
        <?php endif; ?>
        <?php $first = false; ?>

        <?php if (is_string($item)): ?>
            <span class="text-vine"><strong><?= $item?></strong></span>
        <?php endif; ?>


        <?php if (is_array($item)): ?>
            <a class="text-dark-grey no-decoration" href="<?= $item['link']?>"><?= $item['name']?></a>
        <?php endif; ?>


    <?php endforeach;?>




</nav>