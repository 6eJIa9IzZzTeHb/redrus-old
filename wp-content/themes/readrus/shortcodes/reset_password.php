<?php
/**
 * @var $remember bool
 * @var $loginOrEmail string
 * @var $password string
 */
?>
<div class="bg-white text-center ptb100 pull-left full-width">
    <h2 class="text-dark-grey mb40">Восстановление пароля</h2>

    <form method="POST" class="mb20 typical-form">
        <input type="text" name="login" value="<?= $loginOrEmail?>" class="grey-input" placeholder="Логин или email"/>
        <div class="text-danger"><?= $error['loginOrEmail']?></div>
        <br/>
        <button class="btn vine-button">Восстановить</button>
        <br/>

        <h5 class="mt20 text-dark-grey mt40">Вернуться к авторизации на ReadRus</h5>
        <a href="<?= get_permalink(262) ?>">Авторизация</a>
    </form>

</div>