<?php
/**
 * @var $attrs []
 */

$limit = isset($attrs['limit']) ? $attrs['limit'] : 16;
$page = (get_query_var('page')) ? get_query_var('page') : 1;
$type = isset($_GET['type']) ? $_GET['type'] : 'rate';

$famous = isset($attrs['famous']);


$offset = $limit * ($page - 1);
global $wpdb;
$wpdb->show_errors();


$sql = "SELECT count(*)  AS `count` FROM " . $wpdb->prefix . 'posts WHERE post_type="writer" AND post_status="publish"';

$results = $wpdb->get_results($sql);

$count = $results[0]->count;

$pages = ceil($count / $limit);



$sql = "";

switch ($type) {
    case "rate":

        $sql = "SELECT * FROM " . $wpdb->prefix . "posts LEFT JOIN  " . $wpdb->prefix . "poststar_post_rate
        ON " . $wpdb->prefix . "posts.id=" . $wpdb->prefix . "poststar_post_rate.post_id";
        $sql .= ' WHERE post_type="writer" AND post_status="publish" ';
        $sql .= ' ORDER BY  `rate` desc ';


        break;
    case "birthday":
        $sql = "SELECT * FROM " . $wpdb->prefix . 'posts LEFT JOIN (SELECT * FROM  ' . $wpdb->prefix . 'postmeta WHERE meta_key="birthday") as T2
        ON ' . $wpdb->prefix . "posts.id=T2.post_id";
        $sql .= ' WHERE post_type="writer" AND post_status="publish"';
        $sql .= ' ORDER BY  `meta_value` desc ';


        break;
    case "views":
        $sql = "SELECT * FROM " . $wpdb->prefix . 'posts LEFT JOIN (SELECT * FROM  ' . $wpdb->prefix . 'post_views WHERE period="total") as T2
        ON ' . $wpdb->prefix . "posts.id=T2.id";
        $sql .= ' WHERE post_type="writer" AND post_status="publish"';
        $sql .= ' ORDER BY  `count` desc ';

        break;
    case "comments":
        $sql = "SELECT * FROM " . $wpdb->prefix . 'posts LEFT JOIN (SELECT count(*) as `count`, comment_post_ID FROM  ' . $wpdb->prefix . 'comments GROUP BY comment_post_ID) as T2
        ON ' . $wpdb->prefix . "posts.id=T2.comment_post_ID";
        $sql .= ' WHERE post_type="writer" AND post_status="publish"';
        $sql .= ' ORDER BY  `count` desc ';

        break;
    case "books":

        $sql = "SELECT * FROM " . $wpdb->prefix . 'posts LEFT JOIN (SELECT count(*) as `count`, post_author FROM  ' . $wpdb->prefix . 'posts WHERE post_type="book" AND post_status="publish" GROUP BY post_author) as T2
        ON ' . $wpdb->prefix . "posts.id=T2.post_author";
        $sql .= ' WHERE post_type="writer" AND post_status="publish"';
        $sql .= ' ORDER BY  `count` desc ';
        break;
    case "rand":

        $sql = "SELECT * FROM " . $wpdb->prefix . 'posts ';
        $sql .= ' WHERE post_type="writer" AND post_status="publish" ';
        $sql .= ' ORDER BY rand() ';

        break;

}

if ($famous) {

    $sql = "SELECT * FROM " . $wpdb->prefix . 'posts LEFT JOIN (SELECT * FROM  ' . $wpdb->prefix . 'postmeta WHERE meta_key="famous") as T2
        ON ' . $wpdb->prefix . "posts.id=T2.post_id";
    $sql .= ' WHERE post_type="writer" AND post_status="publish" AND meta_value="1"';
    $sql .= ' ORDER BY  `meta_value` desc ';

    $results = $wpdb->get_results($sql);
    $count = $results[0]->count;
}


$sql .= ' LIMIT ' . $limit . ' ';
$sql .= ' OFFSET ' . $offset;
$results = $wpdb->get_results($sql);



?>




<div class="bg-white p50 plr50 pull-left full-width">

    <h3 class="bordered border-grey text-middle-grey">Рейтинг писателей</h3>

</div>
<div class="bg-white p50 plr50 pull-left full-width">
    <div class="row">
        <?php $index = 0; ?>
        <?php foreach ($results as $result): ?>


        <?php $id = $result->ID ?>

        <?php $pic = get_field('writer_avatar', $id); ?>






        <?php if (!$pic) {
            $pic = get_template_directory_uri() . '/img/person.png';
        } else {
            $pic = $pic['url'];
        } ?>
        <?php if ($index++ == 8 && $count > 10): ?>

    </div>
</div>

<div style="background-image:url('<?= of_get_option('main_top_image') ?>');"
     class="landing-main-top-image pull-left"></div>

<div class="bg-white p50 plr50 pull-left full-width">

    <div class="row">
        <?php endif; ?>

        <div class="col-lg-3 col-md-6 col-sm-6">

            <a class="landing-author " href="<?= get_permalink($id)?>">
                <div class="img" style="background: url('<?= $pic ?>')">
                </div>
                <div class="content m0 full-width text-center">
                    <div class="head">
                        <?= $result->post_title ?>
                    </div>

                    <div class="text-middle-grey f20 mt10">Рейтинг: <span class="text-dark-green"
                                                                          data-readrus-post-id="<?= $id ?>"><?= readrus_stars_get_average_for_post($id) ?></span>
                    </div>

                    <div class="author mt10">


                        <?= readrus_stars_for_post($id, false); ?>
                    </div>
                </div>
            </a>

        </div>


        <?php endforeach; ?>


    </div>
</div>



<div class="bg-white p50 plr50 pull-left full-width text-center">

    <?php if ($count > 1): ?>

        <?php $min = max(1, $page-9); ?>
        <?php $max = min($page+9, $pages); ?>
        <?php while ($max-$min+1 > 9) {
            if ($page-$min < $max- $page) {
                $max--;
            } else {
                $min++;
            }
        }?>


        <?php if ($page > 1): ?>
            <a href="?page=<?= $page-1?>&type=<?= $type?>"><span class="fa fa-arrow-left"></span> Назад</a>

            &nbsp;
        <?php endif; ?>

        <?php for($page_i=$min; $page_i <= $max; $page_i++): ?>

            <a href="?page=<?= $page_i?>&type=<?= $type?>" class="btn-typical no-border no-decoration circle plr8 ptb3 <?= $page_i==$page?'active':''?>"><strong><?= $page_i?></strong></a>

        <?php endfor; ?>

        <?php if ($page < $pages): ?>
            &nbsp;
            <a href="?page=<?= $page+1?>&type=<?= $type?>">Далеее <span class="fa fa-arrow-right"></span></a>
        <?php endif; ?>


    <?php endif; ?>

</div>



<div class="mt20 pull-left">&nbsp;</div>
