<?php
/**
 * @var $attrs []
 */

$limit = isset($attrs['limit']) ? $attrs['limit'] : 16;
$page = (get_query_var('page')) ? get_query_var('page') : 1;
$type = isset($_GET['type']) ? $_GET['type'] : 'rate';


$offset = $limit * ($page - 1);
global $wpdb;
$wpdb->show_errors();

$filter = isset($attrs['filter']) ? $attrs['filter'] : '';

$sql = "SELECT count(*)  AS `count` FROM " . $wpdb->prefix . 'posts WHERE post_type="' . $filter . '" AND post_status="publish"';

$results = $wpdb->get_results($sql);

$count = $results[0]->count;

$pages = ceil($count / $limit);


$sql = "SELECT * FROM " . $wpdb->prefix . "posts";

$sql .= ' WHERE post_type="' . $filter . '" AND post_status="publish" ';
$sql .= ' ORDER BY  `id` desc ';


$sql .= ' LIMIT ' . $limit . ' ';
$sql .= ' OFFSET ' . $offset;
$results = $wpdb->get_results($sql);



$sql = "SELECT * FROM " . $wpdb->prefix . "posts";

$sql .= ' WHERE post_type="' . $filter . '" AND post_status="publish" ';
$sql .= ' ORDER BY  `id` desc ';


$sql .= ' LIMIT ' . $limit . ' ';
$sql .= ' OFFSET ' . $offset;
$popular = $wpdb->get_results($sql);

?>




<div class="bg-white p50 plr50 pull-left full-width">

    <h3 class="bordered border-grey text-middle-grey"><?= get_the_title() ?></h3>

</div>
<div class="bg-white p50 plr50 pull-left full-width">

    <div class="row">

        <div class="col-md-8">
            <div class="row">
                <?php if (!$count): ?>
                    <div class="col-md-12">
                        <h3 class="text-center">Нет записей</h3>
                    </div>
                <?php endif; ?>
                <?php $index = 0; ?>
                <?php foreach ($results as $result): ?>


                <?php $id = $result->ID ?>

                <?php $pic = get_field('book_image', $result->ID); ?>


                <?php $genre = implode(', ', wp_get_post_terms($result->ID, 'book_ganre', array("fields" => "names"))) ?>


                <?php $authors = get_field('book_author', $result->ID);


                if (is_array($authors) && sizeof($authors)) {
                    $writers = [];
                    foreach ($authors as $author) {
                        $writers [] = $author->post_title;
                    }
                } else {
                    $writers = ['неизвестный автор'];
                }

                ?>

                <?php
                $date = get_the_date();
                if (!strlen($date)) {
                    $date = "неизвестна";
                } else {
                }

                ?>

                <?php if (!$pic) {
                    $pic = get_template_directory_uri() . '/img/book.png';
                } else {
                    $pic = $pic['url'];
                } ?>
                <?php if ($index++ == 8 && $count > 10): ?>

            </div>
        </div>

        <div style="background-image:url('<?= of_get_option('main_top_image') ?>');"
             class="landing-main-top-image pull-left"></div>

        <div class="bg-white p50 plr50 pull-left full-width">

            <div class="row">
                <?php endif; ?>

                <?php
                $content_post = get_post($id);
                ?>

                <div class="col-lg-12">

                    <a class="book full-width" href="<?= get_permalink($id) ?>">


                        <table class="full-width">
                            <tr>
                                <td STYLE="WIDTH:200px;">
                                    <div class="img" style="border:none; background: url('<?= $pic ?>')">
                                    </div>
                                </td>
                                <td>
                                    <div class="content m0 full-width text-justify"
                                         style="height: auto !important; width:100%">
                                        <div style="border-bottom: 1px solid grey;"
                                             class="text-middle-grey border-grey">
                                            Добавлено: <strong style="color:#333;"><?= $date ?></strong></div>
                                        <h5><?= get_the_title($id) ?></h5>

                                        <p class="text-dark-grey"><?= substr($content_post->post_content, 100) ?></p>

                                        <p style="border-top: 1px solid grey;">
                                    <span
                                        style="border-right: 1px solid grey; padding-right:22px; margin-right:17px;"><span
                                            class="text-vine fa fa-eye"></span> 0</span>
                                            <span><span class="text-vine fa fa-comment"></span> 0</span>
                                            <a class="pull-right" style="text-decoration: none; padding-top:5px;"
                                               href="<?= get_permalink($id) ?>">Читать больше <span
                                                    class="fa fa-arrow-right"></span></a>
                                        </p>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </a>

                </div>



                <?php endforeach; ?>


            </div>
        </div>



        <?php if (sizeof($result)): ?>
        <div class="col-md-4">

            <h5>Популярные статьи</h5>
            <div class="row">

                <?php foreach ($popular as $article): ?>



                    <?php $id = $article->ID;?>
                    <div class="col-md-12" style="border:2px solid rgba(144,144,144,0.2); box-shadow: 1px 1px 1px grey; margin-bottom: 5px;">
                        <h5><?= get_the_title($id) ?></h5>


                        <p style="border-top: 1px solid grey;">
                                    <span
                                        style="border-right: 1px solid grey; padding-right:22px; margin-right:17px;"><span
                                            class="text-vine fa fa-eye"></span> 0</span>
                            <span><span class="text-vine fa fa-comment"></span> 0</span>
                            <a class="pull-right" style="text-decoration: none; padding-top:5px;"
                               href="<?= get_permalink($id) ?>">Читать больше <span
                                    class="fa fa-arrow-right"></span></a>
                        </p>
                    </div>

                <?php endforeach; ?>

            </div>
        </div>
        <?php endif; ?>
        <div class="bg-white p50 plr50 pull-left full-width text-center">

            <?php if ($count > 1): ?>

                <?php $min = max(1, $page - 9); ?>
                <?php $max = min($page + 9, $pages); ?>
                <?php while ($max - $min + 1 > 9) {
                    if ($page - $min < $max - $page) {
                        $max--;
                    } else {
                        $min++;
                    }
                } ?>


                <?php if ($page > 1): ?>
                    <a href="?page=<?= $page - 1 ?>&type=<?= $type ?>"><span class="fa fa-arrow-left"></span> Назад</a>

                    &nbsp;
                <?php endif; ?>

                <?php for ($page_i = $min; $page_i <= $max; $page_i++): ?>

                    <a href="?page=<?= $page_i ?>&type=<?= $type ?>"
                       class="btn-typical no-border no-decoration circle plr8 ptb3 <?= $page_i == $page ? 'active' : '' ?>"><strong><?= $page_i ?></strong></a>

                <?php endfor; ?>

                <?php if ($page < $pages): ?>
                    &nbsp;
                    <a href="?page=<?= $page + 1 ?>&type=<?= $type ?>">Далеее <span
                            class="fa fa-arrow-right"></span></a>
                <?php endif; ?>


            <?php endif; ?>

        </div>
    </div>
</div>
<div class="mt20 pull-left">&nbsp;</div>
