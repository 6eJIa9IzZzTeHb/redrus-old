<?php
/**
 * @var $attrs []
 */

$limit = isset($attrs['limit']) ? $attrs['limit'] : 16;
$page = (get_query_var('page')) ? get_query_var('page') : 1;
$type = isset($_GET['type']) ? $_GET['type'] : 'rate';


$offset = $limit * ($page - 1);
global $wpdb;
$wpdb->show_errors();


$sql = "SELECT count(*)  AS `count` FROM " . $wpdb->prefix . 'posts WHERE post_type="book" AND post_status="publish"';

$results = $wpdb->get_results($sql);

$count = $results[0]->count;

$pages = ceil($count / $limit);


$sql = "";

switch ($type) {
    case "rate":

        $sql = "SELECT * FROM " . $wpdb->prefix . "posts LEFT JOIN  " . $wpdb->prefix . "poststar_post_rate
        ON " . $wpdb->prefix . "posts.id=" . $wpdb->prefix . "poststar_post_rate.post_id";
        $sql .= ' WHERE post_type="book" AND post_status="publish" ';
        $sql .= ' ORDER BY  `rate` desc ';


        break;
    case "birthday":
        $sql = "SELECT * FROM " . $wpdb->prefix . 'posts LEFT JOIN (SELECT * FROM  ' . $wpdb->prefix . 'postmeta WHERE meta_key="birthday") as T2
        ON ' . $wpdb->prefix . "posts.id=T2.post_id";
        $sql .= ' WHERE post_type="writer" AND post_status="publish"';
        $sql .= ' ORDER BY  `meta_value` desc ';


        break;
    case "views":
        $sql = "SELECT * FROM " . $wpdb->prefix . 'posts LEFT JOIN (SELECT * FROM  ' . $wpdb->prefix . 'post_views WHERE period="total") as T2
        ON ' . $wpdb->prefix . "posts.id=T2.id";
        $sql .= ' WHERE post_type="writer" AND post_status="publish"';
        $sql .= ' ORDER BY  `count` desc ';

        break;
    case "comments":
        $sql = "SELECT * FROM " . $wpdb->prefix . 'posts LEFT JOIN (SELECT count(*) as `count`, comment_post_ID FROM  ' . $wpdb->prefix . 'comments GROUP BY comment_post_ID) as T2
        ON ' . $wpdb->prefix . "posts.id=T2.comment_post_ID";
        $sql .= ' WHERE post_type="writer" AND post_status="publish"';
        $sql .= ' ORDER BY  `count` desc ';

        break;
    case "books":

        $sql = "SELECT * FROM " . $wpdb->prefix . 'posts LEFT JOIN (SELECT count(*) as `count`, post_author FROM  ' . $wpdb->prefix . 'posts WHERE post_type="book" AND post_status="publish" GROUP BY post_author) as T2
        ON ' . $wpdb->prefix . "posts.id=T2.post_author";
        $sql .= ' WHERE post_type="writer" AND post_status="publish"';
        $sql .= ' ORDER BY  `count` desc ';
        break;
    case "rand":

        $sql = "SELECT * FROM " . $wpdb->prefix . 'posts ';
        $sql .= ' WHERE post_type="writer" AND post_status="publish" ';
        $sql .= ' ORDER BY rand() ';

        break;

}


$sql .= ' LIMIT ' . $limit . ' ';
$sql .= ' OFFSET ' . $offset;
$results = $wpdb->get_results($sql);



?>




<div class="bg-white p50 plr50 pull-left full-width">

    <h3 class="bordered border-grey text-middle-grey"><?= get_the_title() ?></h3>

</div>
<div class="bg-white p50 plr50 pull-left full-width">
    <div class="row">
        <?php $index = 0; ?>
        <?php foreach ($results as $result): ?>


        <?php $id = $result->ID ?>

        <?php $pic = get_field('book_image', $result->ID); ?>


        <?php $genre = implode(', ', wp_get_post_terms($result->ID, 'book_ganre', array("fields" => "names"))) ?>


        <?php $authors = get_field('book_author', $result->ID);


        if (is_array($authors) && sizeof($authors)) {
            $writers = [];
            foreach ($authors as $author) {
                $writers [] = $author->post_title;
            }
        } else {
            $writers = ['неизвестный автор'];
        }

        ?>

        <?php
        $date = get_field('book_create_time', $result->ID);
        if (!strlen($date)) {
            $date = "неизвестна";
        } else {
            $date = substr($date, 0, 4) . "-" . substr($date, 4, 2) . "-" . substr($date, 6, 2);
            $date = date('m.d.Y', strtotime($date));
        }

        ?>

        <?php if (!$pic) {
            $pic = get_template_directory_uri() . '/img/book.png';
        } else {
            $pic = $pic['url'];
        } ?>
        <?php if ($index++ == 8 && $count > 10): ?>

    </div>
</div>

<div style="background-image:url('<?= of_get_option('main_top_image') ?>');"
     class="landing-main-top-image pull-left"></div>

<div class="bg-white p50 plr50 pull-left full-width">

    <div class="row">
        <?php endif; ?>

        <div class="col-lg-6 col-md-6 col-sm-3">

            <a class="book full-width" href="<?= get_permalink($id) ?>">


                <table>
                    <tr>
                        <td>
                            <div class="img" style="border:none; background: url('<?= $pic ?>')">
                            </div>
                        </td>
                        <td>
                            <div class="content m0 full-width text-justify" style="height: auto !important;">
                                <div class="text-middle-grey">Название:</div>
                                <div class="head">
                                    <?= $result->post_title ?>
                                </div>
                                <div class="text-middle-grey mt15">Форма:</div>
                                <div class="head">
                                    <?= $genre ?>
                                </div>
                                <div class="text-middle-grey mt10">Автор:</div>
                                <div class="head">
                                    <?= implode(', ', $writers) ?>
                                </div>
                                <div class="text-middle-grey">Дата публикации:</div>
                                <div class="head">
                                    <?= $date ?>
                                </div>
                                <div class="head mt20">
                                    <a href="<?= get_permalink($result->ID) ?>" class="btn vine-button">Посомтреть &nbsp;&nbsp;<span
                                            class="fa fa-arrow-right"></span></a>
                                </div>

                            </div>
                        </td>
                    </tr>
                </table>
            </a>

        </div>


        <?php endforeach; ?>


    </div>
</div>


<div class="bg-white p50 plr50 pull-left full-width text-center">

    <?php if ($count > 1): ?>

        <?php $min = max(1, $page - 9); ?>
        <?php $max = min($page + 9, $pages); ?>
        <?php while ($max - $min + 1 > 9) {
            if ($page - $min < $max - $page) {
                $max--;
            } else {
                $min++;
            }
        } ?>


        <?php if ($page > 1): ?>
            <a href="?page=<?= $page - 1 ?>&type=<?= $type ?>"><span class="fa fa-arrow-left"></span> Назад</a>

            &nbsp;
        <?php endif; ?>

        <?php for ($page_i = $min; $page_i <= $max; $page_i++): ?>

            <a href="?page=<?= $page_i ?>&type=<?= $type ?>"
               class="btn-typical no-border no-decoration circle plr8 ptb3 <?= $page_i == $page ? 'active' : '' ?>"><strong><?= $page_i ?></strong></a>

        <?php endfor; ?>

        <?php if ($page < $pages): ?>
            &nbsp;
            <a href="?page=<?= $page + 1 ?>&type=<?= $type ?>">Далеее <span class="fa fa-arrow-right"></span></a>
        <?php endif; ?>


    <?php endif; ?>

</div>


<div class="mt20 pull-left">&nbsp;</div>
