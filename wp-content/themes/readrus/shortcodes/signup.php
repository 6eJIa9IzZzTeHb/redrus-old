<?php
/**
 * @var $remember bool
 * @var $loginOrEmail string
 * @var $password string
 */

$days = [];
$months = [];
$years = [];

for ($i = 1; $i <= 31; $i++) {
    $days[] = ($i < 10 ? '0' : '') . $i;
}
for ($i = 2017; $i >= 1920; $i--) {
    $years[] = $i;
}
$months = [
    'Январь', 'Февраль', 'Март',
    'Апрель', 'Май', 'Июнь',
    'Июль', 'Август', 'Сентябрь',
    'Октябрь', 'Ноябрь', 'Декабрь'
];

function select($name, $value, $placeholder, $list)
{
    $html = "";

    $index = 0;

    $html .= '<select class="plr10 ptb5" name="' . $name . '">';
    $html .= '<option value="' . ($index++) . '">' . $placeholder . '</option>';
    foreach ($list as $label) {
        $html .= '<option '.($index==$value?'selected':'').' value="' . ($index++) . '">' . $label . '</option>';
    }
    $html .= "</select>";

    return $html;

}


function field($name, $value, $error, $placeholder = "", $type = "text")
{

    return '                <input type="' . $type . '" name="' . $name . '" value="' . $value . '" class="grey-input full-width" placeholder="' . $placeholder . '"/>
                <div class="text-danger">' . $error[$name] . '</div>';

}


?>
<div class="bg-white text-center ptb40 pull-left full-width">
    <h2 class="text-dark-grey mb40">Регистрация нового пользователя</h2>

    <form method="POST" class="mb20 plr40 full-width typical-form">
        <div class="row">
            <div class="col-md-12">
                <div class="text-middle-grey text-justify">* - поля обязательные для ввода</div>

            </div>
        </div>
        <div class="row ptb5">
            <div class="col-md-6">
                <?= field('login', $login, $error, 'Логин *') ?>
                <div class="text-middle-grey text-justify pl10">логин необходим для авторизации на сайте</div>

            </div>
            <div class="col-md-6">
                <?= field('email', $email, $error, 'Email *') ?>

            </div>
        </div>
        <div class="row ptb5">
            <div class="col-md-6">
                <?= field('fname', $fname, $error, 'Имя *') ?>
                <div class="text-middle-grey text-justify pl10">В качестве обращения к Вам будут использоваться эти данные </div>

            </div>
            <div class="col-md-6">
                <?= field('sname', $sname, $error, 'Фамилия *') ?>

            </div>
        </div>
        <div class="row ptb5">
            <div class="col-md-6">
                <?= field('password', $password, $error, 'Пароль *', 'password') ?>

            </div>
            <div class="col-md-6">
                <?= field('retry_password', $retry_password, $error, 'Повторите пароль *', 'password') ?>

            </div>
        </div>

        <div class="row ptb5">
            <div class="col-md-6  pt20">
                <div class="text-justify plr10 ptb5">
                    <span class="mt3 inline-block">День рождения
                    </span>
                    <div class="pull-right">
                        <?= select('birth_day', $day, 'День', $days) ?>
                        <?= select('birth_month', $month, 'Месяц', $months) ?>
                        <?= select('birth_year', $year, 'Год', $years) ?>
                    </div>
                    <div class="text-danger mt8 text-center"><?=$error['birth_day']?></div>'
                </div>
            </div>
            <div class="col-md-6 pt20">
                <?= field('city', $city, $error, 'Город проживания') ?>

            </div>
        </div>
        <div class="row">
            <div class="col-md-12 pt20">
                <label class="checkbox hand">

                    <span class="check mt3 inline-block vertical-top">
                        <span class="check-ok fa fa-check relative"></span>
                        <span class="check-fail relative"></span>
                        <input name="remember" type="checkbox" <?= $remember ? 'checked' : '' ?>/>
                    </span>
                    <span class="mt3 inline-block vertical-top text-middle-grey">
                        Я соглашаюсь с пользовательским соглашением, прочёл его полностью и обязаюсь выполнять
                    </span>
                </label>
                <div class="text-danger text-center"><?=$error['remember']?></div>'

            </div>
        </div>
        <br/>
        <button class="btn vine-button">Зарегестрироваться</button>
        <br/>
        <h5 class="mt20 text-dark-grey mt40">У меня есть аккаунт на ReadRus</h5>
        <a href="<?= get_permalink(262) ?>">Войти</a>
    </form>

</div>