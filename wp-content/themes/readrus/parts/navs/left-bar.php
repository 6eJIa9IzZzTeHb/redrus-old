<?php

$page_id = get_the_ID();

$pageUrl = $_SERVER['REQUEST_URI'];

?>
<nav id="l-nav" class="hidden-sm-down">
    <ul>
        <li class="title_menu" id="buttonMenu">
            <div id="menu-toggle">
                <div id="hamburger">
                    <b></b>
                    <b></b>
                    <b></b>
                </div>
                <div id="cross">
                    <b></b>
                    <b></b>
                </div>
            </div>
        </li>

        <li class="subtitle">Меню</li>
        <li class="menu-item menu-icon-home <?= $page_id==1?'active':''?>"><a href="<?= get_home_url()?>"><span>Главная</span></a></li>
        <li class="menu-item menu-icon-pen <?= strpos($pageUrl, '/writer')===0?'active':''?>"><a href="<?= get_permalink(75)?>"><span>Писатели</span></a></li>
        <li class="menu-item menu-icon-books <?= strpos($pageUrl, '/book')===0?'active':''?>"><a href="<?= get_permalink(245)?>"><span>Произведения</span></a></li>
        <li class="menu-item menu-icon-newspaper <?= (
            strpos($pageUrl, '/writer-school')===0 ||
            strpos($pageUrl, '/awards')===0 ||
            strpos($pageUrl, '/articles_and_notes')===0
        )?'active':''?>"><a href="<?= get_permalink(297)?>"><span>Полезные статьи</span></a></li>
        <li class="menu-item menu-icon-medal <?= strpos($pageUrl, 'events')?'active':''?>"><a href="<?= get_permalink(286)?>"><span>Конкурсы</span></a></li>
        <li class="line"></li>

        <li class="subtitle" ><a href="/help"><span>Обратная связь</span></a></li>
        <li class="menu-item menu-icon-security <?= strpos($pageUrl, 'rules')?'active':''?>"><a href="<?= get_permalink(358)?>"><span>Правила сайта</span></a></li>
        <li class="menu-item menu-icon-adv-0 <?= strpos($pageUrl, 'adv')?'active':''?>"><a href="<?= get_permalink(283)?>"><span>Реклама</span></a></li>
        <li class="menu-item menu-icon-dialog <?= strpos($pageUrl, 'contats')?'active':''?>"><a href="<?= get_permalink(362)?>"><span>Контакты</span></a></li>
        <li class="line"></li>

        <li class="copyright">
            <span>Copyright © ReadRus <?= date('Y')?></span>
            <span>Все права защищены</span>
        </li>

        <!--        <li id="menu-item-601" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-601"><a href="/" title="Главная"><span>Главная</span></a></li>-->
        <!--        <li id="menu-item-585" class="menu-potables menu-item menu-item-type-post_type menu-item-object-page menu-item-585"><a href="/potables/" title="Напитки"><span>Напитки</span></a></li>-->
        <!--        <li id="menu-item-537" class="menu-delivery menu-item menu-item-type-post_type menu-item-object-page menu-item-537"><a href="/delivery/" title="Доставка"><span>Доставка</span></a></li>-->
        <!--        <li id="menu-item-39" class="menu-sale menu-item menu-item-type-custom menu-item-object-custom menu-item-39"><a href="/#sale" title="Акции"><span>Акции</span></a></li>-->
        <!--        <li id="menu-item-40" class="menu-reviews menu-item menu-item-type-custom menu-item-object-custom menu-item-40"><a href="/#reviews" title="Отзывы"><span>Отзывы</span></a></li>-->
    </ul>
</nav>
