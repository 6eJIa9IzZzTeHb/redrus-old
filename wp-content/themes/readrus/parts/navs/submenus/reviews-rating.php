<?php
/**
 * @var $attrs []
 */

$items = [
    'rate'=>'По рейтингу',
    'birthday'=>'По дате рождения',
    'views'=>'По количеству просмотров',
    'comments'=>'По количеству комментариев',
    'books'=>'По количеству произведений',
    'rand'=>'В случайном порядке',
];


$field = "type";

$activeId = isset($_GET[$field])?$_GET[$field]:'rate';

?>
<nav class="bg-light-grey plr50  relative box-shadow-grey1 z1 pt10 pull-left full-width">

    <a href="<?= get_permalink(298)?>" class="submenu-link pull-left <?= $attrs['active']==1?'active':''?>"><?= get_the_title(298)?></a>
    <a href="<?= get_permalink(295)?>" class="submenu-link pull-left <?= $attrs['active']==2?'active':''?>"><?= get_the_title(295)?></a>
<!--    <a href="--><?//= get_permalink(296)?><!--" class="submenu-link pull-left --><?//= $attrs['active']==3?'active':''?><!--">--><?//= get_the_title(296)?><!--</a>-->
    <a href="<?= get_permalink(297)?>" class="submenu-link pull-left <?= $attrs['active']==4?'active':''?>"><?= get_the_title(297)?></a>



</nav>