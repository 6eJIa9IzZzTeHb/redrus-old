<?php
/**
 * @var $attrs []
 */

$items = [
    'rate'=>'По рейтингу',
    'birthday'=>'По дате рождения',
    'views'=>'По количеству просмотров',
    'comments'=>'По количеству комментариев',
    'books'=>'По количеству произведений',
    'rand'=>'В случайном порядке',
];


$field = "type";

$activeId = isset($_GET[$field])?$_GET[$field]:'rate';

?>
<nav class="bg-light-grey plr50  relative box-shadow-grey1 z1 pt10 pull-left full-width">

    <a href="<?= get_permalink(286)?>" class="submenu-link pull-left <?= $attrs['active']==1?'active':''?>"><?= get_the_title(286)?></a>
    <a href="<?= get_permalink(285)?>" class="submenu-link pull-left <?= $attrs['active']==2?'active':''?>"><?= get_the_title(285)?></a>


    <?php if (isset($attrs['additional'])): ?>
        <div class="pull-right text-middle-grey mt10">
            Отображать:
            <div class="dropdown inline-block">
                <button style="width:260px;" class="bordered b1 text-left carret-right w200 border-grey dropdown-toggle no-decoration bg-white text-dark-grey" id="page_submenu" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <?= $items[$activeId]?>
                </button>

                <div class="dropdown-menu" aria-labelledby="page_submenu">

                    <?php foreach($items as $key=>$label): ?>
                        <a class="dropdown-item no-decoration  text-middle-grey" href="?<?= $field?>=<?= $key?>"><?= $label?></a>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    <?php endif ?>
</nav>