<?php
/**
 * @var $attrs []
 */

$items = [
    'rate'=>'По рейтингу',
    'birthday'=>'По дате рождения',
    'views'=>'По количеству просмотров',
    'comments'=>'По количеству комментариев',
    'books'=>'По количеству произведений',
    'rand'=>'В случайном порядке',
];


$field = "type";

$activeId = isset($_GET[$field])?$_GET[$field]:'rate';

?>
<nav class="bg-light-grey plr50  relative box-shadow-grey1 z1 pt10 pull-left full-width">

    <a href="<?= get_permalink(245)?>" class="submenu-link pull-left <?= $attrs['active']==1?'active':''?>"><?= get_the_title(245)?></a>
    <a href="<?= get_permalink(246)?>" class="submenu-link pull-left <?= $attrs['active']==2?'active':''?>"><?= get_the_title(246)?></a>
    <a href="<?= get_permalink(247)?>" class="submenu-link pull-left <?= $attrs['active']==3?'active':''?>"><?= get_the_title(247)?></a>


    <div class="pull-right text-middle-grey mt10">
        <div>
            <span class="fa fa-filter"></span>
            <span>
                Открыть фильтр <span class="fa fa-angle-down"></span>
            </span>
        </div>
    </div>
</nav>