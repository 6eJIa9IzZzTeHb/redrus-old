<?php

$page_id = get_the_ID();

$user = wp_get_current_user();
?>

<nav class="nav-top">


    <div class="container">
        <div class="bg-cream panel">


            <div class="logo">
                <a href="/"><img src="<?= get_template_directory_uri() . "/img/logo.png" ?>" alt=""/></a>
            </div>

            <div class="search-panel hidden-sm-down">
                <form action="/search" class="">
                    <label class="m-0 no-wrap" for="">
                        <input name="search"
                               class="pull-left ptb2 no-bg no-border soft-input border-hover-vine plr20 ptb8 f13"
                               style="width:220px; padding-top:7px;" type="text" placeholder="Что будем искать?"/>
                        <button
                            class="fa fa-search pull-left text-white ptb10 plr20 bg-vine no-border vine vine-button">
                            &nbsp;&nbsp;Искать
                        </button>
                    </label>
                </form>
            </div>


            <div class="login-button text-center">


                <nav class="navbar navbar-toggleable-md navbar-light ptb-0">
                    <?php if ($user->ID == 0): ?>
                        <a href="<?= $user->ID ? get_permalink(348) . '?nickname=' . $user->nickname : get_permalink(262) ?>"
                           class="btn-typical br20 plr13 ptb5 no-wrap auth-button no-decoration">
                            <span class="circle b5 border-vine menu-icon-auth"></span>
                            <span class="hidden-auto ml10">АВТОРИЗАЦИЯ</span>
                        </a>
                    <?php else: ?>

                        <div class="br20 plr13 ptb5 no-wrap auth-button no-decoration">
                            <?php $pic = get_field('avatar', 'user_' . $user->ID) ?>
                            <?php if (!$pic) $pic = get_avatar_url($user->ID); ?>
                            <span class="circle border-vine bg-img inline-block mt2"
                                  style="width:28px; height:29px; vertical-align: top; background-image: url('<?= $pic ?>')"></span>

                            <div class="ml10 inline-block" style="vertical-align: top;">
                                <div class="hidden-md-down">
                                    <div class="mt-5"><?= $user->nickname ?></div>
                                    <div
                                        class="f13 mt-5"><?= $user->nickname == 'readrus' ? 'администратор' : '' ?></div>
                                </div>
                            </div>
                            <span class=" hidden-md-down">
                            <a href="<?= get_permalink(353) ?>"
                               class="fa fa-sign-out no-decoration inline-block ml10 f25"></a>
                            </span>
                        </div>
                    <?php endif; ?>

                    <button class="navbar-toggler navbar-toggler-right btn-typical  hidden-md-up" type="button"
                            data-toggle="collapse" data-target="#navbarTogglerDemo02"
                            aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="fa fa-bars"></span>
                    </button>
                    <div class="collapse navbar-collapse bg-cream" id="navbarTogglerDemo02">
                        <ul class="navbar-nav mr-auto mt-2 mt-md-0">
                            <li class="menu-item menu-icon-home <?= $page_id == 1 ? 'active' : '' ?>"><a href="/"><span>Главная</span></a>
                            </li>
                            <li class="menu-item menu-icon-pen"><a
                                    href="<?php the_permalink(159); ?>"><span>Писатели</span></a></li>
                            <li class="menu-item menu-icon-books"><a
                                    href="<?php echo get_post_type_archive_link('post_publication'); ?>"><span>Произведения</span></a>
                            </li>
                            <li class="menu-item menu-icon-newspaper"><a
                                    href="<?php echo get_post_type_archive_link('s_writing'); ?>"><span>Полезные статьи</span></a>
                            </li>
                            <li class="menu-item menu-icon-medal"><a
                                    href="<?php echo get_post_type_archive_link('post_contest'); ?>"><span>Конкурсы</span></a>
                            </li>
                            <li class="line"></li>

                            <li class="subtitle">-- Обратная связь --</li>
                            <li class="menu-item menu-icon-security"><a href="<?php the_permalink(51); ?>"><span>Правила сайта</span></a>
                            </li>
                            <li class="menu-item menu-icon-adv-0"><a
                                    href="<?php the_permalink(10); ?>"><span>Реклама</span></a></li>
                            <li class="menu-item menu-icon-dialog"><a
                                    href="<?php the_permalink(103); ?>"><span>Контакты</span></a></li>
                        </ul>
                    </div>

                </nav>
            </div>


        </div>
    </div>
</nav>