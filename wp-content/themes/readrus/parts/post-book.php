<?php

the_post();

$pic = get_field('book_image');

$id = get_the_ID();


if (!$pic) {
    $pic = get_template_directory_uri() . '/img/book.png';
} else {
    $pic = $pic['url'];
}

$data = readrus_stars_for_post();

?>
<main class="content mt-20 mlr-20 pl-5 full-width ">
    <div class="container">

        <?= do_shortcode('[breadcrumbs submenu="Произведения" submenu_link="/book-previews"]') ?>




        <div class="bg-white pt50 plr50 pull-left full-width">


            <div class="landing-author pull-left bg-white mr20 pt10 text-center"
                 style="width:250px; height:350px;">
                <div class="img" style="background: url('<?= $pic ?>'); width:220px; height:250px;">
                </div>
                <div class="text-center mt10 full-width pull-left">

                    <span class="pull-left">&nbsp; <span class="fa fa-eye text-vine"></span> <?= pvc_get_post_views() ?></span>
                    <span class="pull-right f15 text-middle-grey">&nbsp; (<?= readrus_stars_get_count_for_post() ?>) &nbsp;</span>
                    <span class="text-middle-grey f15 pull-right">Рейтинг: <span class="text-dark-green"
                                                                                 data-readrus-post-id="<?= $id ?>"><?= readrus_stars_get_average_for_post($id) ?></span>
                    </span>
                </div>

                <div class="author mt15 pull-left full-width text-center">


                    <?= readrus_stars_for_post($id); ?>
                </div>
            </div>

            <div style="padding-left:290px;" class="mb20">
                <h2 class="text-middle-grey"
                    style="border-bottom: 1px solid rgba(13,13,13,0.3)"><?= get_the_title() ?></h2>
                <?php $genre = implode(', ', wp_get_post_terms(get_the_ID(), 'book_ganre', array("fields" => "names"))) ?>
                <?php $form = implode(', ', wp_get_post_terms(get_the_ID(), 'book_form', array("fields" => "names"))) ?>

                <div class="row text-center">
                    <div class="col-md-3 col-sm-6">
                        <h5 class="text-dark-grey">Жанр:</h5>

                        <div class="text-dark-grey f20"><strong><?= $genre ?></strong></div>
                    </div>
                    <div class="col-md-3 col-sm-6">

                        <h5 class="text-dark-grey">Форма:</h5>

                        <div class="text-dark-grey f20">
                            <strong>
                                <?= $form ?></strong>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">

                        <?php $authors = get_field('book_author');


                        if (is_array($authors) && sizeof($authors)) {
                            $writers = [];
                            foreach ($authors as $author) {
                                $writers [] = $author->post_title;
                            }
                        } else {
                            $writers = ['неизвестный автор'];
                        }

                        ?>
                        <h5 class="text-dark-grey">Автор:</h5>

                        <div class="text-dark-grey f20">
                            <strong>
                                <?= implode(', ', $writers) ?></strong></div>
                    </div>
                    <div class="col-md-3 col-sm-6">

                        <h5 class="text-dark-grey">Дата написания:</h5>

                        <div class="text-dark-grey f20">
                            <strong><?= get_the_date() ?></strong></div>
                    </div>
                </div>

                <h5 class="text-middle-grey mt20" style="border-bottom: 1px solid rgba(13,13,13,0.3)">
                    <strong>Анотация:</strong></h5>


            </div>

            <div class="text-justify"><?= get_the_content() ?></div>


        </div>
        <div class="bg-white p50 pb100 plr50 pull-left full-width mb40">
            <div class="box-shadow-grey1">
                <a href="#" class="submenu-link active">Комментарии</a>
                <a href="#" class="submenu-link">Рецензии</a>


            </div>

            <div class="text-center">
                Нет данных
            </div>

        </div>
</main>
