<?php

the_post();

$pic = get_field('writer_avatar');
$land_pic = get_field('writer_landing_img');

$id = get_the_ID();

if (!$land_pic) {
    $land_pic = get_template_directory_uri() . '/img/authors-bg.png';
} else {
    $land_pic = $land_pic['url'];
}


if (!$pic) {
    $pic = get_template_directory_uri() . '/img/person.png';
} else {
    $pic = $pic['url'];
}

$data = readrus_stars_for_post();

?>
<main class="content mt-20 mlr-20 pl-5 full-width ">
    <div class="container">

        <?= do_shortcode('[breadcrumbs submenu="Рейтинг писаталей" submenu_link="/writers-rating"]') ?>


        <div class="bg-img page-img" style="background-image: url(<?= $land_pic ?>)"></div>


        <div class="bg-white p50 plr50 pull-left full-width mb40">

            <div class="landing-author pull-left bg-white mr20 pt10 text-center"
                 style="margin-top:-150px; width:250px; height:350px;">
                <div class="img" style="background: url('<?= $pic ?>'); width:220px; height:250px;">
                </div>
                <div class="text-center mt10 full-width pull-left">

                    <span class="pull-left">&nbsp; <span class="fa fa-eye text-vine"></span> <?= pvc_get_post_views() ?></span>
                    <span class="pull-right f15 text-middle-grey">&nbsp; (<?= readrus_stars_get_count_for_post() ?>) &nbsp;</span>
                    <span class="text-middle-grey f15 pull-right">Рейтинг: <span class="text-dark-green"
                                                                                 data-readrus-post-id="<?= $id ?>"><?= readrus_stars_get_average_for_post($id) ?></span>
                    </span>
                </div>

                <div class="author mt15 pull-left full-width text-center">


                    <?= readrus_stars_for_post($id, false); ?>
                </div>
            </div>

            <div style="padding-left:290px;" class="mb20">
                <h2 class="text-middle-grey"
                    style="border-bottom: 1px solid rgba(13,13,13,0.3)"><?= get_the_title() ?></h2>

                <div class="row text-center">
                    <div class="col-md-3 col-sm-6">
                        <h5 class="text-dark-grey">Дата рождения:</h5>

                        <div class="text-dark-grey f20"><strong><?= get_field('birthday') ?></strong></div>
                    </div>
                    <div class="col-md-3 col-sm-6">

                        <h5 class="text-dark-grey">Возраст:</h5>

                        <div class="text-dark-grey f20">
                            <strong><?= intval(date('Y')) - intval(substr(get_field('birthday'), 0, 4)) ?></strong>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">

                        <h5 class="text-dark-grey">Город:</h5>

                        <div class="text-dark-grey f20">
                            <strong><?= get_field('city') ? get_field('city') : "неизвестен" ?></strong></div>
                    </div>
                    <div class="col-md-3 col-sm-6">

                        <h5 class="text-dark-grey">Веб-сайт:</h5>

                        <div class="text-dark-grey f20">
                            <strong><?= get_field('website') ? get_field('website') : "site.com" ?></strong></div>
                    </div>
                </div>

                <h5 class="text-middle-grey mt20" style="border-bottom: 1px solid rgba(13,13,13,0.3)">
                    <strong>Биография:</strong></h5>


            </div>

            <div class="text-justify"><?= get_the_content() ?></div>


            <div class="row">
                <div class="col-md-6">

                    <h5 class="text-middle-grey mt20" style="border-bottom: 1px solid rgba(13,13,13,0.3)"><strong>Произведения
                            автора</strong></h5>


                    <div class="row">
                        <?php
                        global $wpdb;
                        $sql = "SELECT * FROM " . $wpdb->prefix . "postmeta WHERE `meta_key`='book_author' AND `meta_value` LIKE \"%2:\\\"" . $id . "\\\"%\"";


                        $results = $wpdb->get_results($sql);

                        ?>
                        <?php if ($size = sizeof($results)): ?>
                            <?php $count = 0; ?>
                            <?php foreach ($results as $result): ?>

                                <?php $postR = get_post($result->post_id); ?>

                                <?php $pic = get_field('book_image', $postR->ID); ?>
                                <?php if (!$pic) {
                                    $pic = get_template_directory_uri() . '/img/book.png';
                                } else {
                                    $pic = $pic['url'];
                                } ?>

                                <div class="col-md-6 <?= $count>3?'none':''?>">
                                    <a href="" class="authors_book no-decoration inline-block  text-center m10">
                                        <span class="img bg-image" style="background: url('<?= $pic ?>');"></span>
                                        <span
                                            class="text-middle-grey text-center f18"><?= $postR->post_title ?></span>
                                    </a>
                                </div>



                                <?php if (++$count == 4 && $size > 4): ?>
                                    <div class="col-md-12 text-center">
                                        <button class="text-white ptb10 plr20 bg-vine no-border vine vine-button mt20" data-toggle="more">Показать ещё <?= $size-4?></button>
                                    </div>
                                <?php endif; ?>

                            <?php endforeach; ?>
                        <?php else: ?>
                            <div class="col-md-12">Не найдено записей</div>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="col-md-6">

                    <h5 class="text-middle-grey mt20" style="border-bottom: 1px solid rgba(13,13,13,0.3)"><strong>Статьи
                            и заметки автора</strong></h5>



                    <div class="row">
                        <?php
                        global $wpdb;
                        $sql = "SELECT * FROM `" . $wpdb->prefix . "postmeta`".
                        "INNER JOIN " . $wpdb->prefix . "posts
                        ON `" . $wpdb->prefix . "postmeta`.`post_id` = `" . $wpdb->prefix . "posts`.`ID`".
                         "WHERE `meta_key`='author' AND `meta_value`=".$id." AND `post_type`='post'";


                        $results = $wpdb->get_results($sql);

                        ?>
                        <?php if ($size = sizeof($results)): ?>
                            <?php $count = 0; ?>
                            <?php foreach ($results as $result): ?>



                                <?php $postR = get_post($result->post_id); ?>

                                <?php $pic = get_field('book_image', $postR->ID); ?>
                                <?php if (!$pic) {
                                    $pic = get_template_directory_uri() . '/img/book.png';
                                } else {
                                    $pic = $pic['url'];
                                } ?>

                                <div class="col-md-12 <?= $count>3?'none':''?> box-shadow-grey1 mtb15 pb5">
                                    <h5 class="text-middle-grey"><strong><?= $postR->post_title?></strong>

                                        <span class="pull-right">&nbsp; <span class="fa fa-eye text-vine"></span> <?= pvc_get_post_views($postR->ID) ?></span>

                                    </h5>
                                    <div style="min-height:100px;">
                                    <?= $postR->post_excerpt?>
                                    </div>
                                    <div>
                                        <div class="row">
                                            <div class="col-md-4 text-left">
                                                <span class="fa fa-calendar text-fine"></span> <?= date("d.m.Y", strtotime($postR->post_date))?>
                                            </div>
                                            <div class="col-md-4 text-center">
                                                <span class="fa fa-comment-o text-fine"></span> <?= $postR->comment_count?>
                                            </div>

                                            <div class="col-md-4 text-right">
                                                <a href="<?= get_permalink($postR->ID)?>">Подробнее <span class="fa fa-arrow-right"></span></a>
                                            </div>
                                        </div>


                                    </div>

                                </div>



                                <?php if (++$count == 4 && $size > 4): ?>
                                    <div class="col-md-12 text-center">
                                        <button class="text-white ptb10 plr20 bg-vine no-border vine vine-button mt20" data-toggle="more">Показать ещё <?= $size-4?></button>
                                    </div>
                                <?php endif; ?>

                            <?php endforeach; ?>
                        <?php else: ?>
                            <div class="col-md-12">Не найдено записей</div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>


    </div>
</main>
