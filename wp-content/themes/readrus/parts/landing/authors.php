<?php

$args = array(
    'post_type' => 'writer',
    'orderby' => 'rand',
    'posts_per_page' => 4,
);


$count = 0;


$the_query = new WP_Query($args);

?>


<?php if ($the_query->have_posts()): ?>
    <div class="row">

        <?php while ($the_query->have_posts()):
    $the_query->the_post();?>

    <?php $id = get_the_ID();?>

    <?php $pic = get_field('writer_avatar'); ?>






    <?php if (!$pic) {
    $pic = get_template_directory_uri() . '/img/person.png';
} else {
    $pic = $pic['url'];
}?>


    <div class="col-lg-6 col-md-12 col-sm-6">

        <div class="landing-author ">
            <div class="img" style="background: url('<?= $pic ?>')">
            </div>
            <div class="content m0">
                <div class="head">
                    <?= the_title() ?>
                </div>

                <div class="text-middle-grey f20 mt10">Рейтинг: <span class="text-dark-green" data-readrus-post-id="<?= get_the_ID()?>"><?= readrus_stars_get_average_for_post()?></span></div>

                <div class="author mt10">


                        <?= readrus_stars_for_post(get_the_ID(), false);?>
                </div>
            </div>
        </div>

    </div>





    <?php if (++$count==4) break; ?>


    <?php wp_reset_postdata(); ?>
<?php endwhile; ?>

    </div>

<?php endif; ?>
<?php if(!$count): ?>
    <h4 class="text-danger"><i>нет новых произведений</i></h4>
<?php endif; ?>