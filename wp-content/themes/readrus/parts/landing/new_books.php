<?php

$args = array(
    'post_type' => 'book',
    'orderby' => 'date',
    'posts_per_page' => 100,
);

$count = 0;

$the_query = new WP_Query($args);

?>


<?php if ($the_query->have_posts()): ?>

    <?php while ($the_query->have_posts()):
        $the_query->the_post();?>

        <?php $id = get_the_ID();?>

        <?php if (get_field('book_anounce')) continue;?>


        <?php $pic = get_field('book_image'); ?>

        <?php $authors = get_field('book_author');


        if (is_array($authors) && sizeof($authors)) {
            $writers = [];
            foreach ($authors as $author) {
                $writers [] = $author->post_title;
            }
        } else {
            $writers = ['неизвестный автор'];
        }

        ?>


        <?php
        $date = get_field('book_create_time');
        if (!strlen($date)) {
            $date = "неизвестна";
        } else {
            $date = substr($date, 0, 4) . "-" . substr($date, 4, 2) . "-" . substr($date, 6, 2);
            $date = date('m.d.Y', strtotime($date));
        }

        ?>


        <?php if (!$pic) {
        $pic = get_template_directory_uri() . '/img/book.png';
    } else {
        $pic = $pic['url'];
    }?>



        <?php ?>
        <div class="book">
            <div class="img" style="background: url('<?= $pic ?>')">
            </div>
            <div class="content">
                <div>
                    <span class="fa fa-calendar text-vine date-field"> <?= $date ?></span>
                </div>
                <div class="head">
                    <?= the_title() ?>
                </div>
                <div class="author">
                    Автор(ы): <?= implode(', ', $writers) ?>
                </div>

                <div>
                    <a class="btn btn-typical plr15 mt10 ptb5" href="<?= get_permalink() ?>">Посмотреть &nbsp;&nbsp;<span class="fa fa-arrow-right f15"></span> </a>

                </div>
            </div>
        </div>






        <?php wp_reset_postdata(); ?>



        <?php if (++$count==4) break; ?>

    <?php endwhile; ?>


<?php endif; ?>
<?php if(!$count): ?>
    <h4 class="text-danger"><i>нет новых произведений</i></h4>
<?php endif; ?>