<?php

$args = array(
    'post_type' => 'book',
    'orderby' => 'rand',
    'posts_per_page' => 100,
);


$count = 0;


$the_query = new WP_Query($args);

?>


<?php if ($the_query->have_posts()): ?>
    <div class="row">

        <?php while ($the_query->have_posts()):
    $the_query->the_post();?>

    <?php $id = get_the_ID();?>

    <?php $pic = get_field('book_image'); ?>

    <?php if (get_field('book_anounce')) continue;?>

    <?php $authors = get_field('book_author');


          if (is_array($authors) && sizeof($authors)) {
              $writers = [];
              foreach ($authors as $author) {
                $writers []= $author->post_title;
              }
          } else {
              $writers = ['неизвестный автор'];
          }

    ?>



    <?php if (!$pic) {
    $pic = get_template_directory_uri() . '/img/book.png';
} else {
    $pic = $pic['url'];
}?>


    <div class="col-lg-6 col-md-12 col-sm-6">

        <a class="book" href="<?= get_permalink() ?>">
            <div class="img" style="background: url('<?= $pic ?>')">
            </div>
            <div class="content">
                <div class="head">
                    <?= the_title() ?>
                </div>
                <div class="author">
                    <?= implode(', ', $writers)?>
                </div>
            </div>
        </a>

    </div>



    <?php if (++$count==4) break; ?>


    <?php wp_reset_postdata(); ?>
<?php endwhile; ?>

    </div>

<?php endif; ?>
<?php if(!$count): ?>
    <h4 class="text-danger"><i>нет новых произведений</i></h4>
<?php endif; ?>