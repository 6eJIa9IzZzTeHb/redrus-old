<?php

$args = array(
    'post_type' => 'book',
    'orderby' => 'rand',
    'posts_per_page' => 4,
);

$count = 0;

$the_query = new WP_Query($args);

?>


<?php if ($the_query->have_posts()): ?>

    <?php while ($the_query->have_posts()):
        $the_query->the_post();?>

        <?php $id = get_the_ID();?>



        <?php $pic = get_field('book_image'); ?>

        <?php $authors = get_field('book_author');


        if (is_array($authors) && sizeof($authors)) {
            $writers = [];
            foreach ($authors as $author) {
                $writers [] = $author->post_title;
            }
        } else {
            $writers = ['неизвестный автор'];
        }

        ?>


        <?php
        $date = get_field('book_create_time');
        if (!strlen($date)) {
            $date = "неизвестна";
        } else {
            $date = substr($date, 0, 4) . "-" . substr($date, 4, 2) . "-" . substr($date, 6, 2);
            $date = date('m.d.Y', strtotime($date));
        }

        ?>


        <?php if (!$pic) {
        $pic = get_template_directory_uri() . '/img/book.png';
    } else {
        $pic = $pic['url'];
    }?>



        <?php ?>
        <div class="book">
            <div class="img" style="background: url('<?= $pic ?>')">
            </div>
            <div class="content">
                <div>
                    <span class="fa fa-calendar text-grey date-field">
                    <?= the_title() ?></span>
                </div>
                <div class="head">
                </div>
                <div class="author">

                    <div class="text-middle-grey f14 mtb2">Рейтинг: <span class="text-dark-green" data-readrus-post-id="<?= get_the_ID()?>"><?= readrus_stars_get_average_for_post()?></span></div>

                    <div class="author mtb5">


                        <?= readrus_stars_for_post();?>
                    </div>
                    Автор(ы): <?= implode(', ', $writers) ?>
                </div>

                <div>
                    <a class="btn btn-typical plr15 mt10 ptb5" href="<?= get_permalink() ?>">Посмотреть &nbsp;&nbsp;<span class="fa fa-arrow-right f15"></span> </a>

                </div>
            </div>
        </div>






        <?php wp_reset_postdata(); ?>



        <?php if (++$count==4) break; ?>

    <?php endwhile; ?>


<?php endif; ?>
<?php if(!$count): ?>
    <h4 class="text-danger"><i>нет новых произведений</i></h4>
<?php endif; ?>