<?php

$args = array(
    'post_type' => 'post',
    'orderby' => 'rand',
    'posts_per_page' => 4,
);



$the_query = new WP_Query($args);

?>


<?php if ($the_query->have_posts()): ?>
    <div class="row">

        <?php while ($the_query->have_posts()):
    $the_query->the_post();?>

    <?php
        $id = get_the_ID();

        $author = get_field('author');

        $pic = get_field('writer_avatar', $author->ID);


    ?>



    <?php if (!$pic) {
    $pic = get_template_directory_uri() . '/img/person.png';
} else {
    $pic = $pic['url'];
}?>


    <div class="col-lg-6 col-md-12">

        <div class="landing-post d-flex flex-row text-grey" >
            <div class="img" style="flex: 0 0 195px; background: url('<?= $pic ?>')">
            </div>
            <div class="content" style="flex: 1 1 100px">
                <div class="head">
                    <?= $author->post_title ?>
                    <div class="pull-right f15"> <span class="fa fa-calendar text-vine"></span> <?= get_the_date()?></div>
                </div>
                <div class="author">
                    <?= get_the_excerpt()?>

                    <br/>
                    <br/>
                    <a href="<?= get_permalink() ?>" class="text-vine pull-right">подробнее &nbsp;&nbsp;&nbsp;<span class="fa fa-arrow-right"></span></a>
                </div>
            </div>
        </div>

    </div>




    <?php wp_reset_postdata(); ?>
<?php endwhile; ?>

    </div>

<?php endif; ?>
<?php if(!$count): ?>
    <h4 class="text-danger"><i>нет новых произведений</i></h4>
<?php endif; ?>