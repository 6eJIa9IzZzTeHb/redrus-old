jQuery(function () {

    function refreshFuels() {

        $('.fuel').each(function () {
            $(this).css({width: $(this).data('default')});
        });
    }

    refreshFuels();


    var ajaxSended = false;
    $(".stars.editable .star").click(function (e) {

        if (ajaxSended) return;
        ajaxSended = false;

        var options = $(this).data('readrus-star');

        $.ajax({
            url: wp_ajax_object.ajax_url,
            method:"POST",
            dataType:'json',
            data: $.extend({"action": "readrus_stars_ajax_update"}, options),
            success: function (data, t) {
                console.log(data);
                console.log('[data-readrus-post-id='+data.id+']');
                $('[data-readrus-post-id='+data.id+']').html(data.average);
            },
            error: function (a, b, c) {
                console.error(a, b, c);
            }

        });

        $(this).parent().parent().find('.fuel').data('default', options.val / options.max * 100 + "%");
        refreshFuels();


        e.preventDefault();
        return false;

    })
        .mouseenter(function () {
            var options = $(this).data('readrus-star');

            $(this).parent().parent().find('.fuel').css({width: options.val / options.max * 100 + "%"})

        })
        .mouseleave(function () {
            refreshFuels();
        })
        .mouseout(function () {
            refreshFuels();
        });


});