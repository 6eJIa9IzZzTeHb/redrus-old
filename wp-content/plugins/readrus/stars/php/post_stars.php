<?php
/**
 * @var $rate double
 * @var $editable bool
 * @var $post_id int
 */

$max = readrus_stars_get_max($post_id);
?>
<div class="stars <?= $editable?'editable':''?>" style="width:<?= $max*16?>px">
    <div class="fuel" data-default="<?= floor(100 * $rate / $max)?>%"></div>
    <div class="stars-image"></div>

    <?php if ($editable): ?>

        <div class="stars-cnt">
        <?php for($i = 0; $i< $max; $i++): ?>

            <div class="star" data-readrus-star='<?= json_encode(["id"=>$post_id, "val"=>$i+1, "max"=>$max])?>'></div>

        <?php endfor; ?>
        </div>

    <?php endif; ?>

</div>