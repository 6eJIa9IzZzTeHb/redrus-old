<?php

function readrus_stars_get_max($post_id = NULL)
{
    return 10;
}


function readrus_stars_init()
{
    global $wpdb;

    $wpdb->show_errors(true);
    $sql = "CREATE TABLE IF NOT EXISTS `" . $wpdb->prefix . "poststar`";
    $sql .= "(
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `user_ip` varchar(100) NOT NULL DEFAULT '',
  `post_id` int NOT NULL,
  `rate` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY(`id`)
  )";

    $wpdb->query($sql);

    $sql = "CREATE TABLE IF NOT EXISTS `" . $wpdb->prefix . "poststar_post_rate`";
    $sql .= "(
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int NOT NULL,
  `rate` real(8,4),
  `count` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY(`id`)
  )";

    $wpdb->query($sql);


}


function readrus_stars_for_post($post_id = NULL, $editable = true)
{

    if (!$post_id) {
        $post_id = get_the_ID();
    }

    $rate = readrus_stars_get_average_for_post($post_id);
    if ($userRate = readrus_stars_get_user_val($post_id)) {
        $rate = $userRate;
    }

    ob_start();
    include(__DIR__ . "/php/post_stars.php");
    $content = ob_get_contents();
    ob_end_clean();

    return $content;


}


function readrus_stars_update($post_id=NULL, $mark)
{

    if (!$post_id) $post_id = get_the_ID();
    $id = get_current_user_id();

    global $wpdb;
    $wpdb->show_errors(true);


    $average = 0;
    $count = 0;
    $insert = true;
    {
        $sql = "SELECT * FROM `" . $wpdb->prefix . "poststar_post_rate` WHERE `post_id`=" . $post_id;

        $results = $wpdb->get_results($sql);

        if (sizeof($results) != 0) {
            $average = $results[0]->rate;
            $count = $results[0]->count;
            $insert = false;
        }
    }

    $user_mark = readrus_stars_get_user_val($post_id);

    if ($user_mark) {
        if ($count>1) {
            $average = ($average * $count - $user_mark) / ($count - 1);
        } else {
            $average = 0;
        }
        $count--;

    }


    $average = ($average*$count + $mark)/($count+1);
    $count++;

    if ( ! empty( $_SERVER['HTTP_CLIENT_IP'] ) ) {
//check ip from share internet
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif ( ! empty( $_SERVER['HTTP_X_FORWARDED_FOR'] ) ) {
//to check ip is pass from proxy
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }

    $average = number_format($average, 2, '.', '');

    if (!$user_mark) {
        $sql = "INSERT INTO `".$wpdb->prefix . "poststar` (user_id,user_ip,post_id,rate)
VALUES ('".$id."', '".$ip."', '".$post_id."', '".$mark."')" ;
    } else {
        $sql = "UPDATE `".$wpdb->prefix . "poststar` SET rate='".$mark."' WHERE ((user_id=".$id.") OR  (user_id=0 AND user_ip='".$ip."')) AND post_id=".$post_id;
    }
    $wpdb->query($sql);

    if ($insert) {
        $sql = "INSERT INTO `".$wpdb->prefix . "poststar_post_rate` (post_id,rate,count)
VALUES ('".$post_id."', '".$average."', '".$count."')" ;
    } else {
        $sql = "UPDATE `".$wpdb->prefix . "poststar_post_rate` SET rate='".$average."', count='".$count."' WHERE post_id=".$post_id;
    }
    $wpdb->query($sql);
    setcookie('stars_'.$post_id, $mark);

    return $average;
}

function readrus_stars_get_user_val($post_id = NULL)
{
    if (!$post_id) $post_id = get_the_ID();
    if ($id = get_current_user_id()) {
        global $wpdb;
        $sql = "SELECT * FROM `" . $wpdb->prefix . "poststar` WHERE `post_id`=".$post_id." AND `user_id`=".$id;

        $results = $wpdb->get_results($sql);

        if (sizeof($results) == 0) return 0;

        return number_format($results[0]->rate, 2);
    } else if (isset($_COOKIE['stars_'.$post_id])) return $_COOKIE['stars_'.$post_id];

    return 0;
}


function readrus_stars_get_average_for_post($post_id = NULL)
{
    if (!$post_id) $post_id = get_the_ID();

    global $wpdb;
    $sql = "SELECT * FROM `" . $wpdb->prefix . "poststar_post_rate` WHERE `post_id`=".$post_id;

    $results = $wpdb->get_results($sql);

    if (sizeof($results) == 0) return 0;

    return number_format($results[0]->rate, 2);
}


function readrus_stars_get_count_for_post($post_id = NULL)
{
    if (!$post_id) $post_id = get_the_ID();

    global $wpdb;
    $sql = "SELECT * FROM `" . $wpdb->prefix . "poststar_post_rate` WHERE `post_id`=".$post_id;

    $results = $wpdb->get_results($sql);

    if (sizeof($results) == 0) return 0;

    return $results[0]->count;
}



// Same handler function...
add_action('wp_ajax_readrus_stars_ajax_update', 'readrus_stars_ajax_update');
add_action('wp_ajax_nopriv_readrus_stars_ajax_update', 'readrus_stars_ajax_update');

function readrus_stars_ajax_update()
{
    $average = readrus_stars_update($_POST['id'], $_POST['val']);
    echo json_encode(
        array_merge($_POST,
            ['average' => $average]
        ));
    wp_die();
}

function readrus_stars_print_into_the_footer()
{
    wp_enqueue_script('readrus_star_ajax-update', plugins_url('/js/rearus_stars.js', __FILE__), array('jquery'));

    wp_localize_script('readrus_star_ajax-update', 'wp_ajax_object',
        array('ajax_url' => admin_url('admin-ajax.php')));
}

add_action('wp_footer', 'readrus_stars_print_into_the_footer');
