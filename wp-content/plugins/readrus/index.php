<?php
/**
 * Plugin Name: readrus
*/


$modules = ['stars'];


foreach ($modules as $module) {
    include(__DIR__."/stars/". $module .".php");
    register_activation_hook( __FILE__, 'readrus_'.$module.'_init' );
}

